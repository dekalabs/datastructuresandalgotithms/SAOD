#include "cycle_list.hpp"
#include <cmath>
#include <ctime>
#include <iostream>

using namespace std;

void
generateCycleList(CycleList& in)
{
    size_t count = rand() % 10 + 3; // 3 - 12
    cout << "Размер сгенерированого списка : " << count << "\n";
    for (size_t i = 0; i < count; i++) {
        TypeData new_number =
          static_cast<TypeData>(rand() % 10000 - 5000) / 1000.f;
        if (in.contains(new_number)) { //По условию число дожно быть уникальным
            i--;
            continue;
        } else {
            in.push_back(new_number);
        }
    }
    in.print();
}

char
inputMenu()
{
    while (true) {
        cout << "Выберите действие: \n\t"
                "1 - Добавить элемент\n\t"
                "2 - Удалить элемент\n\t"
                "3 - Создать последовательности и вывести их\n\t"
                "4 - Выход\n\t"
                "\n";
        char in;
        cin >> in;
        if (in == '1' || in == '2' || in == '3' || in == '4')
            return in;
        cout << "Такого пункта меню нет\n";
    }
}

void
addElement(CycleList& list)
{
    cout << "Режим добавления элемента: \n"
            " * Элемент будет добавлен после указанного числа.\n"
            " * Если указано 0, то добавленный элемент станет новой опорной "
            "точкой.\n";
    TypeData inData;
    offset_type offset;

    while (true) { // Цикл запроса числа для добавления
        cout << "Введите число для добавления: \n";
        cin >> inData;
        if (cin.fail() || cin.bad()) {
            cin.ignore(cin.rdbuf()->in_avail());
            cin.clear();
            cout << "Неверный ввод\n";
            continue;
        } else if (list.contains(inData)) {
            cout << "Число должно быть уникальным.\n";
            continue;
        } else
            break;
    }

    while (true) { // Цикл запроса позиции для вставки
        cout << "Введите положение вставки относительно нулевого элемента:\n";
        cin >> offset;
        if (cin.fail() || cin.bad()) {
            cin.ignore(cin.rdbuf()->in_avail());
            cin.clear();
            cout << "Неверный ввод\n";
            continue;
        } else {
            break;
        }
    }

    //Проверка на уникальность

    list.insert(offset, inData);
    list.print();
}
void
removeElement(CycleList& list)
{
    if (list.empty()) {
        cout << "Режим удаления не доступен, так как список пуст.\n";
        return;
    }
    cout << "Режим удаления : \n"
            " * Будет удален элемент, который вы указали\n"
            " * Если удаляется 0 элемент, следующий будет новое отправной "
            "точкой.\n";
    offset_type offset;
    while (true) {
        cout << "Введите положение удаления относительно нулевого элемента:\n";
        cin >> offset;
        if (cin.fail() || cin.bad()) {
            cin.ignore(cin.rdbuf()->in_avail());
            cin.clear();
            cout << "Неверный ввод\n";
            continue;
        } else {
            break;
        }
    }

    list.remove(offset);
    list.print();
}

void
invertZnaks(TypeData& n)
{
    n = -n;
}

void
module(TypeData& n)
{
    n = abs(n);
}

bool
isEqual(TypeData r, TypeData l)
{
    r *= pow(10, 4);
    int rval = int(round(r));
    l *= pow(10, 4);
    int lval = int(round(l));
    if (rval == lval)
        return true;
    return false;
}

void
process(CycleList& list)
{
    auto firstCopy = CycleList(list);
    firstCopy.for_each(invertZnaks);
    cout << "С инвертированными знаками: ";
    firstCopy.print();

    auto secondCopy = CycleList(list);
    secondCopy.for_each(module);
    cout << "С значениями по модулю: ";
    secondCopy.print();

    auto thirdList = CycleList();
    for (offset_type i = 0; i < static_cast<offset_type>(firstCopy.size());
         i++) {

        TypeData result = firstCopy.at(i) + secondCopy.at(i);
        if (isEqual(result, 0.0f))
            continue;
        thirdList.push_back(result);
    }
    cout << "Поэлементное сложений 1 двух: ";
    thirdList.print();
}

int
main()
{
    srand(static_cast<unsigned int>(time(nullptr)));

    CycleList list;
    generateCycleList(list);

    bool shouldExit = false;

    while (!shouldExit) {

        char menuIn = inputMenu();
        switch (menuIn) {
            case '1':
                addElement(list);
                break;
            case '2':
                removeElement(list);
                break;
            case '3':
                process(list);
                break;
            case '4':
                shouldExit = true;
                break;
            default:
                cout << "Пункт меню введен не верно.\n";
                break;
        }
    }

    return 0;
}

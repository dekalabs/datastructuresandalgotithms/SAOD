#include "cycle_list.hpp"
#include <stdexcept>

CycleList::CycleList()
  : m_begin(nullptr)
{}

CycleList::CycleList(const CycleList& other)
  : m_begin(nullptr)
{
    for (int i = 0; i < other.size(); i++) {
        TypeData& new_data = other.at(i);
        this->push_back(new_data);
    }
}

CycleList::~CycleList()
{
    //Начинаем с конца, чтобы не изменять m_begin постоянно
    for (offset_type i = this->size() - 1; i >= 0; i--) {
        this->remove(i);
    }
}

TypeData&
CycleList::at(offset_type offset) const
{
    if (!m_begin)
        throw std::runtime_error("Попытка получить значение из пустого списка");
    auto currentNode = m_begin;
    if (offset > 0) {
        for (offset_type i = 0; i < offset; i++) {
            if (currentNode->next == nullptr)
                throw std::runtime_error("CycleList сломан");
            currentNode = currentNode->next;
        }
    } else if (offset < 0) {

        for (offset_type i = 0; offset < i; i--) {
            if (currentNode->back == nullptr)
                throw std::runtime_error("CycleList сломан");
            currentNode = currentNode->back;
        }
    } else
        return m_begin->data;
    return currentNode->data;
}

void
CycleList::insert(offset_type pos, const TypeData& data)
{
    if (!m_begin) {
        auto new_node = new Node();
        new_node->next = new_node;
        new_node->back = new_node;
        new_node->data = data;
        m_begin = new_node;
        return;
    }
    auto currentNode = m_begin;
    if (pos > 0) {
        for (offset_type i = 0; i < pos; i++) {
            if (currentNode->next == nullptr)
                throw std::runtime_error("CycleList сломан");
            currentNode = currentNode->next;
        }
    } else if (pos < 0) {

        for (offset_type i = 0; pos < i; i--) {
            if (currentNode->back == nullptr)
                throw std::runtime_error("CycleList сломан");
            currentNode = currentNode->back;
        }
    } else {
        currentNode = m_begin;
    }

    auto new_node = new Node();
    new_node->next = currentNode->next;
    new_node->back = currentNode;
    new_node->data = data;
    currentNode->next->back = new_node;
    currentNode->next = new_node;

    if (pos == 0) {
        m_begin = new_node;
    }
}

void
CycleList::push_back(const TypeData& new_data)
{
    insert(-1, new_data);
}

void
CycleList::remove(offset_type offset)
{
    if (!m_begin)
        return;

    auto currentNode = m_begin;
    if (offset > 0) {
        for (offset_type i = 0; i < offset; i++) {
            if (currentNode->next == nullptr)
                throw std::runtime_error("CycleList сломан");
            currentNode = currentNode->next;
        }
    } else if (offset < 0) {
        for (offset_type i = 0; offset < i; i--) {
            if (currentNode->back == nullptr)
                throw std::runtime_error("CycleList сломан");
            currentNode = currentNode->back;
        }
    } else {
        currentNode = m_begin;
    }

    if (currentNode == m_begin) {
        if (currentNode->next == m_begin) { ///< Удаляется последний
            m_begin = nullptr;
        } else {
            m_begin = currentNode->next;
        }
    }

    auto lastNode = currentNode->back;
    auto nextNode = currentNode->next;
    lastNode->next = nextNode;
    nextNode->back = lastNode;
    delete currentNode;
}

void
CycleList::for_each(void (*func)(TypeData& data))
{
    for (offset_type i = 0; i < this->size(); i++) {
        func(this->at(i));
    }
}

size_t
CycleList::size() const
{
    if (!m_begin)
        return 0;
    size_t size = 1;
    auto currentNode = m_begin;
    while (currentNode->next != m_begin) {
        currentNode = currentNode->next;
        size++;
    }
    return size;
}

void
CycleList::print() const
{
    if (!m_begin) {
        std::cout << "Нет элементов\n";
        return;
    }
    std::cout << "Предметы в списке : \n";
    auto currentNode = m_begin;
    while (currentNode->next != m_begin) {
        std::cout << currentNode->data << " ";
        currentNode = currentNode->next;
    };
    std::cout << currentNode->data << "\n";
}

bool
CycleList::contains(const TypeData& data)
{
    auto currentNode = m_begin;
    for (size_t i = 0; i < this->size(); i++) {
        if (currentNode->data == data)
            return true;
        currentNode = currentNode->next;
    }
    return false;
}

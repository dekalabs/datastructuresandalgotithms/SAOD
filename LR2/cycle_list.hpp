#ifndef CYCLE_LIST
#define CYCLE_LIST

#include <iostream>

typedef long offset_type;
typedef float TypeData;

struct Node
{
    TypeData data;
    Node* next = nullptr;
    Node* back = nullptr;
};

class CycleList
{
  private:
    Node* m_begin;

  public:
    CycleList();
    CycleList(const CycleList& other);
    ~CycleList();

    TypeData& at(offset_type offset) const;

    void insert(offset_type pos, const TypeData& data);
    void push_back(const TypeData& new_data);
    void remove(offset_type offset);

    void for_each(void (*func)(TypeData& data));

    size_t size() const;
    bool empty() const { return !m_begin; }
    void print() const;

    bool contains(const TypeData& data);
};


#endif //! CYCLE_LIST

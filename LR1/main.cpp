#include <ctime>
#include <iostream>

using namespace std;

int*
generateArray(int count)
{
    int* new_array = new int[count];

    for (int i = 0; i < count; i++) {
        new_array[i] = rand() % count - ((count / 2) + 1);
    }

    return new_array;
}

int
divideByTwoIfEven(int number)
{
    if (number % 2 == 0)
        number /= 2;
    return number;
}

void
incrementIfZero(int check, int* value)
{
    if (check == 0)
        (*value)++;
}

void
printArray(int* mas, int count)
{
    cout << "Array : ";
    for (int i = 0; i < count; i++) {
        cout << mas[i] << " ";
    }
    cout << "\n";
}

int
main()
{
    srand(static_cast<unsigned int>(time(nullptr)));
    int count = rand() % 15 + 10; // 10 - 25

    int* array = generateArray(count);
    printArray(array, count);

    char input = 0;
    while (true) {
        cout << "Выберите действие : \n\t1 - Поделить на 2 все четные "
                "элементы.\n\t2 - Узнать число нулевых элементов\n";

        cin >> input;
        if (cin.fail()) {
            cin.clear();
            cin.ignore(cin.rdbuf()->in_avail());
            cout << "Неверный ввод\n";
        } else {
            if (input == '1' || input == '2')
                break;
            cout << "Такого пункта меню нет\n";
        }
    }

    int sum = 0;

    for (int i = 0; i < count; i++) {
        switch (input) {
            case '1':
                array[i] = divideByTwoIfEven(array[i]);
                break;
            case '2':
                incrementIfZero(array[i], &sum);
                break;
        }
    }

    switch (input) {
        case '1':
            printArray(array, count);
            break;
        case '2':
            cout << "Число нулевых элементов : " << sum << "\n";
            break;
    }

    delete[] array;
    return 0;
}

cmake_minimum_required(VERSION 3.12)

project(LR4)

set(LR4_HDRS
    hashtable.hpp
    list.hpp
    )
set(LR4_SRC
    main.cpp
    hashtable.cpp
    )

add_executable(LR4 ${LR4_HDRS} ${LR4_SRC})
add_executable(LR4_test main_test.cpp hashtable.cpp)

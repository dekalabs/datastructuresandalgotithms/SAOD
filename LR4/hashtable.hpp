#ifndef HASHTABLE_HPP
#define HASHTABLE_HPP

#include "list.hpp"
#include <string>

typedef List<std::string> StringList;

#define KEY_SIZE 6
#define SECTOR_COUNT 1500

class HashTable
{
    StringList* m_table;

  public:
    HashTable();
    ~HashTable();

    bool validateKey(const std::string& key) const;
    int hash(const std::string& key) const;

    void addToTable(const std::string& key);
    StringList& find(const std::string& key) const;
    StringList& find(const int& sector) const;
    void destroy(const std::string& key);

    void outTable(std::ostream& os) const;
    void outCollision(std::ostream& os) const;

    void testHashFunction(const char* outFile = "out.txt",
                          size_t keysCount = 10000) const;
};

#endif // HASHTABLE_HPP

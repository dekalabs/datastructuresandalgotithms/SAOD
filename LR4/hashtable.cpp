#include "hashtable.hpp"
#include <ctime>
#include <fstream>
#include <iostream>

HashTable::HashTable()
  : m_table(nullptr)
{
    m_table = new StringList[SECTOR_COUNT];
}

HashTable::~HashTable()
{
    delete[] m_table;
}

bool
HashTable::validateKey(const std::string& key) const
{
    if (key.size() != KEY_SIZE)
        return false;
    if (isdigit(key[0]) && isdigit(key[1]) && isupper(key[2]) &&
        isupper(key[3]) && isdigit(key[4]) && isdigit(key[5])) {
        return true;
    }
    return false;
}

int
HashTable::hash(const std::string& key) const
{
    if (!validateKey(key))
        throw std::invalid_argument("Invalid key");

    int hash = 0;
    hash += (key[0] + key[1] + key[4]) % 15 * 100; // 00XX0X -> 00XX-14XX
    hash += (key[2] + key[3]) % 10 * 10;           // XXAAXX -> XX0X-XX0X
    hash += key[5] % 10;                           // XXXXX0 -> XXX0-XXX9

    return hash;
}

void
HashTable::addToTable(const std::string& key)
{
    int index = this->hash(key);
    if (!(0 <= index && index < SECTOR_COUNT))
        throw std::runtime_error("Hash function is invalid");
    StringList& list = m_table[index];
    if (!list.isEmpty()) {
        std::cout << "Элемент '" << key << "' имеет коллизии : ";
        size_t listSize = list.size();
        for (size_t i = 0; i < listSize; i++) {
            std::cout << list.at(i) << " ";
        }
        std::cout << "\n";
    }
    list.push_back(key);
}

StringList&
HashTable::find(const std::string& key) const
{
    int index = this->hash(key);
    if (!(0 <= index && index < SECTOR_COUNT))
        throw std::runtime_error("Hash function is invalid");
    return m_table[index];
}

StringList&
HashTable::find(const int& sector) const
{
    if (0 <= sector && sector < SECTOR_COUNT)
        return m_table[sector];
    throw std::runtime_error("Invalid sector");
}

void
HashTable::destroy(const std::string& key)
{
    int index = this->hash(key);
    if (!(0 <= index && index < SECTOR_COUNT))
        throw std::runtime_error("Hash function is invalid");
    StringList& list = m_table[index];

    size_t pos = 0;

    if (!list.find(key, &pos)) {
        std::cout << "Нечего удалять с ключом '" << key << "'\n";
        return;
    }

    list.remove(pos);

    if (!list.isEmpty()) {
        std::cout << "Коллизии с удаленным элементом : ";
        size_t listSize = list.size();
        for (size_t i = 0; i < listSize; i++) {
            std::cout << list.at(i) << " ";
        }
        std::cout << "\n";
    }
}

void
HashTable::outTable(std::ostream& os) const
{
    os << "Хэш-таблица : \n";
    os << "Хэш\tЧисло коллизий\t\tЭлементы\n";
    for (size_t i = 0; i < SECTOR_COUNT; i++) {
        StringList& list = m_table[i];
        size_t listSize = list.size();
        os << i << "\t";
        if (listSize > 1) {
            os << listSize - 1 << "\t\t";
        } else {
            os << "0\t\t";
        }
        if (list.isEmpty()) {
            os << "Нет элементов\n";
            continue;
        }
        size_t sizeList = list.size();
        for (size_t j = 0; j < sizeList; j++) {
            os << list.at(j) << " ";
        }
        os << "\n";
    }
}

void
HashTable::outCollision(std::ostream& os) const
{
    for (size_t i = 0; i < SECTOR_COUNT; i++) {
        StringList& list = m_table[i];
        os << i << "\t" << list.size();
    }
}

void
HashTable::testHashFunction(const char* outFile, size_t keysCount) const
{
    std::ofstream file(outFile);
    if (!file.is_open()) {
        std::cout << "TEST: Невозможно открыть файл \n";
        return;
    }

    srand(static_cast<unsigned int>(time(nullptr)));

    List<std::string> generatedKeys;
    List<int> collisions;

    //Добавление начальных элементов

    for (size_t i = 0; i < SECTOR_COUNT; i++)
        collisions.push_back(0);

    for (size_t count = 0; count < keysCount; count++) {
        std::cout << "TEST : генерируется " << count << " ключ\n";
        std::string key = "00AA00";
        do {

            //Генерирование ключа
            key[0] = '0' + rand() % 10;
            key[1] = '0' + rand() % 10;
            key[2] = 'A' + rand() % 26;
            key[3] = 'A' + rand() % 26;
            key[4] = '0' + rand() % 10;
            key[5] = '0' + rand() % 10;

        } while (generatedKeys.find(key)); //Ключа существовать недолжно.
        generatedKeys.push_back(key);

        int index = hash(key);
        collisions.at(
          static_cast<size_t>(index))++; //Добавляем к числу элементов 1
    }

    //Выводим в файл.

    for (size_t i = 0; i < SECTOR_COUNT; i++) {
        std::cout << "TEST : записываем " << i << " сектор\n";
        file << i << ", " << collisions.at(i) << "\n";
        file.flush();
    }
}

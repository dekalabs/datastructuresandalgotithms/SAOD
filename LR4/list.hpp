#ifndef LIST_HPP
#define LIST_HPP

#include <stdexcept>

template<class TypeData>
struct Node
{
    TypeData data;
    Node* next;
};

template<class TypeData>
class List
{
  private:
    Node<TypeData>* m_begin;

  public:
    List();
    List(const List& other);
    ~List();

    void insert(size_t index, TypeData data);
    void push_back(TypeData data) { this->insert(this->size() - 1, data); }
    void remove(size_t index);

    bool find(TypeData data, size_t* position = nullptr) const;

    TypeData& at(size_t index) const;
    size_t size() const;
    bool isEmpty() const { return !m_begin; }
};

// Realization

template<class TypeData>
List<TypeData>::List()
  : m_begin(nullptr)
{}

template<class TypeData>
List<TypeData>::List(const List& other)
  : List()
{
    size_t otherSize = other.size();
    for (size_t pos = 0; pos < otherSize; pos++) {
        TypeData data = other.at(pos);
        this->push_back(data);
    }
}

template<class TypeData>
List<TypeData>::~List()
{
    while (!this->isEmpty()) {
        this->remove(0);
    }
}

template<class TypeData>
void
List<TypeData>::insert(size_t index, TypeData data)
{
    if (this->isEmpty()) {
        Node<TypeData>* new_node = new Node<TypeData>;
        new_node->data = data;
        new_node->next = new_node;
        m_begin = new_node;
    } else {
        Node<TypeData>* current = m_begin;
        for (size_t pos = 0; pos < index; pos++) {
            current = m_begin->next;
        }

        Node<TypeData>* new_node = new Node<TypeData>;
        new_node->data = data;
        new_node->next = current->next;
        current->next = new_node;
    }
}

template<class TypeData>
void
List<TypeData>::remove(size_t index)
{
    if (this->isEmpty())
        return;
    Node<TypeData>* current = m_begin;

    size_t preindex = index - 1;

    if (index == 0) { //Если удаляем нулевой, то идем до
                      //последнего элемента списка.
        preindex = this->size() - 1;
    }

    for (size_t pos = 0; pos < preindex; //< Идем на элемент перед удаляемым
         pos++) {
        current = m_begin->next;
    }

    Node<TypeData>* toDel = current->next;
    current->next = toDel->next;

    if (toDel == m_begin) {
        if (toDel->next == m_begin)
            m_begin = nullptr;
        else
            m_begin = toDel->next;
    }
    delete toDel;
}

template<class TypeData>
bool
List<TypeData>::find(TypeData data, size_t* position) const
{
    if (isEmpty())
        return false;
    size_t size = this->size();

    Node<TypeData>* current = m_begin;
    for (size_t pos = 0; pos < size; pos++) {
        if (current->data == data) {
            if (position)
                *position = pos;
            return true;
        }
        current = current->next;
    }

    return false;
}

template<class TypeData>
TypeData&
List<TypeData>::at(size_t index) const
{
    if (this->isEmpty())
        throw std::runtime_error("Accesing to empty list");

    Node<TypeData>* current = m_begin;
    for (size_t pos = 0; pos < index; pos++) {
        current = current->next;
    }
    return current->data;
}

template<class TypeData>
size_t
List<TypeData>::size() const
{
    if (this->isEmpty())
        return 0;

    Node<TypeData>* current = m_begin;
    size_t size = 1;
    while (current->next != m_begin) {
        current = current->next;
        size++;
    }
    return size;
}

#endif // LIST_HPP

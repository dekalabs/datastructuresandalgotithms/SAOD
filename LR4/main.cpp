#include "hashtable.hpp"
#include <fstream>
#include <iostream>

using namespace std;

char
inputMenu();

std::string
inputKey(HashTable& table);

int
inputSector();

void
addElement(HashTable& table);

void
searchBySector(HashTable& table);

void
searchByKey(HashTable& table);

void
destroyElement(HashTable& table);

void
outToFile(HashTable& table);

int
main()
{

    HashTable hashTable;
    bool shouldExit = false;

    while (!shouldExit) {
        char input = inputMenu();
        switch (input) {
            case '1':
                addElement(hashTable);
                break;
            case '2':
                searchBySector(hashTable);
                break;
            case '3':
                searchByKey(hashTable);
                break;
            case '4':
                destroyElement(hashTable);
                break;
            case '5':
                hashTable.outTable(cout);
                break;
            case '6':
                outToFile(hashTable);
                break;
            case '7':
                hashTable.testHashFunction("test.txt", 5000);
                break;
            case '8':
                shouldExit = true;
                break;
            default:
                cout << "Пункт меню введен не верно.\n";
                break;
        }
    }

    return 0;
}

char
inputMenu()
{

    cout << "Выберите действие: \n\t"
            "1 - Добавить элемент в таблицу\n\t"
            "2 - Поиск в хэш таблице по сегменту\n\t"
            "3 - Поиск в хэш таблице по ключу\n\t"
            "4 - Удалить элемент\n\t"
            "5 - Вывод хэш таблицы\n\t"
            "6 - Вывод хэш таблицы в файл 'out.txt'\n\t"
            "7 - Тестирование коллизий с выводом в test.txt\n\t"
            "8 - Выход\n\t"
            "\n";
    char in;
    cin >> in;

    return in;
}

std::string
inputKey(HashTable& table)
{
    std::string key = "";
    do {
        std::cout << "Введите ключ формата ццББцц: ";
        std::cin >> key;
    } while (!table.validateKey(key));

    return key;
}

int
inputInt()
{
    cin.clear();
    cin.ignore(cin.rdbuf()->in_avail());
    cin.sync();

    std::cout << "Введите сектор от 0 до 1499 : ";

    char ch;
    while (true) {
        std::string tmpNumber;
        cin.get(ch);
        while (ch != '\n') {
            tmpNumber += ch;
            cin.get(ch);
        }
        try {
            int timed = std::stoi(tmpNumber);
            if (!(0 <= timed && timed < SECTOR_COUNT)) {
                continue;
            }
            return timed;
        } catch (const std::exception& e) {
            (void)e;
            std::cout << "Введите сектор от 0 до 1499 : ";
            continue;
        }
    }
}

void
addElement(HashTable& table)
{
    std::string key = inputKey(table);

    table.addToTable(key);
}

void
searchBySector(HashTable& table)
{
    int sec = inputInt();
    StringList& list = table.find(sec);

    std::cout << "В секторе '" << sec << "' : ";
    size_t listSize = list.size();

    if (listSize == 0) {
        std::cout << "нет элементов\n";
        return;
    }

    for (size_t pos = 0; pos < listSize; pos++) {
        std::cout << list.at(pos) << " ";
    }
    std::cout << "\n";
}

void
searchByKey(HashTable& table)
{
    std::string key = inputKey(table);

    StringList& list = table.find(key);

    std::cout << "В ключе '" << key << "' с номером сектора '"
              << table.hash(key) << "' ";
    size_t listSize = list.size();

    if (listSize == 0) {
        std::cout << "нет элементов\n";
        return;
    }

    for (size_t pos = 0; pos < listSize; pos++) {
        std::cout << list.at(pos) << " ";
    }
    std::cout << "\n";
}

void
destroyElement(HashTable& table)
{
    std::string key = inputKey(table);
    table.destroy(key);
}

void
outToFile(HashTable& table)
{
    std::ofstream file("out.txt");
    if (!file.is_open()) {
        std::cout << "Невозможно открыть файл : out.txt\n";
        return;
    }
    table.outTable(file);
}

#include "bintree.hpp"
#include <iostream>
#include <string>

BinTree::BinTree()
  : m_root(nullptr)
{}

BinTree::~BinTree()
{
    while (!empty()) {
        remove(m_root);
    }
}

void
BinTree::add(BinTreeData data)
{
    std::cout << "---Добавление '" << data << "'---\n";

    auto newNode = new BinNode();
    newNode->data = data;

    if (empty()) {
        m_root = newNode;
        return;
    }

    auto currentNode = m_root;

    while (currentNode) {
        if (data <= currentNode->data) {
            if (currentNode->left) {
                currentNode = currentNode->left;
                continue;
            } else {
                currentNode->left = newNode;
                newNode->parent = currentNode;
                break;
            }
        } else {
            if (currentNode->right) {
                currentNode = currentNode->right;
                continue;
            } else {
                currentNode->right = newNode;
                newNode->parent = currentNode;
                break;
            }
        }
    }

    normalize(newNode);

    std::cout << "После добавления:";
    print();
}

BinNode*
BinTree::find(BinTreeData data) const
{
    std::cout << "---Поиск '" << data << "'---\n";
    size_t steps = 0;
    auto currentNode = m_root;
    while (true) {
        steps++;

        if (data < currentNode->data) {
            if (currentNode->left) {
                currentNode = currentNode->left;
            } else {
                std::cout << "Поиск не дал результатов, потратив " << steps
                          << " шагов\n";
                return nullptr; //Не найдено
            }
        } else if (currentNode->data < data) {
            if (currentNode->right) {
                currentNode = currentNode->right;
            } else {
                std::cout << "Поиск не дал результатов, потратив " << steps
                          << " шагов\n";
                return nullptr; //Не найдено
            }
        } else {
            std::cout << "Поиск обнаружил искомое значение - " << currentNode
                      << ", потратив " << steps << " шагов\n";
            return currentNode;
        }
    }
}

void
BinTree::remove(BinTreeData data)
{
    std::cout << "-----Удаление по значению '" << data << "'-----\n";
    auto pos = find(data);
    if (!pos) {
        std::cout << "Нечего удалять.\n";
        return;
    }

    remove(pos);
}

void
BinTree::print() const
{
    if (empty()) {
        std::cout << "\nДерево : пустое \n";
        return;
    };

    std::cout << "\nДерево: \n";

    struct NodeWithPath
    {
        BinNode* node;
        std::string path;
    };

    size_t level = 0;

    auto currentLevel = new Queue<NodeWithPath>();
    auto nextLevel = new Queue<NodeWithPath>();

    nextLevel->push({ m_root, "" });

    while (!nextLevel->empty()) {
        //Перенос в текущей уровень из последующего
        while (!nextLevel->empty()) {
            currentLevel->push(nextLevel->pop());
        }

        std::cout << "Уровень " << level << " : \n";

        while (!currentLevel->empty()) {
            auto node = currentLevel->pop();
            if (!node.node)
                continue;

            std::cout << "\t " << node.path << " : " << node.node->data;

            if (node.node->left) {
                std::string path = node.path;
                path += 'L';
                nextLevel->push({ node.node->left, path });
            }
            if (node.node->right) {
                std::string path = node.path;
                path += 'R';
                nextLevel->push({ node.node->right, path });
            }
        }

        std::cout << "\n";

        level++;
    }

    delete currentLevel;
    delete nextLevel;
}

bool
BinTree::contains(BinNode* node) const
{
    if (!node)
        return false;
    auto listOfElements = makeQueueInWidth();
    while (!listOfElements->empty()) {
        auto nodeList = listOfElements->pop();
        if (node == nodeList) {
            delete listOfElements;
            return true;
        }
    }
    delete listOfElements;
    return false;
}

int
BinTree::parentTo(BinNode* parent, BinNode* node) const
{
    if (!parent || !node || (!parent->left && !parent->right))
        return 0;
    if (parent->left == node) {
        return -1;
    } else if (parent->right == node) {
        return 1;
    } else {
        return 0;
    }
}

void
BinTree::removeOdds()
{
    auto mainList = makeQueueInWidth();
    while (!mainList->empty()) {
        auto node = mainList->pop();
        if (contains(node)) {
            remove(node);
        }
        if (!mainList->empty()) {
            mainList->pop(); //Для нечетности просто
                             //проигнорируем один элемент
        }
    }

    delete mainList;
}

void
BinTree::remove(BinNode* node)
{
    std::cout << "---Удаление '" << node->data << "'(" << node << ")---\n";

    if (leaf(node)) {
        if (node == m_root) {
            m_root = nullptr;
        } else {
            auto parent = node->parent;
            int code = parentTo(parent, node);

            if (code == -1) {
                parent->left = nullptr;
            } else if (code == 1) {
                parent->right = nullptr;
            }

            normalize(parent);
        }
        delete node;

    } else if (node->right) {

        auto curNode = node->right;

        while (curNode->left) {
            curNode = curNode->left;
        }

        //Смена местами нодов
        swap(curNode, node);

        remove(node);
    } else {
        auto newNode = node->left;
        auto parent = node->parent;
        newNode->parent = parent;

        if (!parent) {
            m_root = newNode;
        } else {

            int code = parentTo(parent, node);
            if (code == -1) {
                parent->left = newNode;
            } else if (code == 1) {
                parent->right = newNode;
            } else {
                throw std::runtime_error(
                  "Imposible to have no parent at this situation");
            }
        }

        delete node;
    }
    std::cout << "После удаления:";
    print();
}

void
BinTree::swap(BinNode* node1, BinNode* node2)
{
    if (node1 == node2)
        return;
    std::cout << "--Замена местами " << node1->data << "(" << node1 << ") с "
              << node2->data << "(" << node2 << ").--\n";

    auto parent1 = node1->parent;
    auto parent2 = node2->parent;

    if (parent1) {
        int code = parentTo(parent1, node1);

        if (code == -1)
            parent1->left = node2;
        else if (code == 1)
            parent1->right = node2;
        else
            throw std::runtime_error(
              "Imposible to have no parent at this situation");
    } else {
        m_root = node2;
    }

    if (parent2) {
        int code = parentTo(parent2, node2);

        if (code == -1)
            parent2->left = node1;
        else if (code == 1)
            parent2->right = node1;
        else
            throw std::runtime_error(
              "Imposible to have no parent at this situation");
    } else {
        m_root = node1;
    }

    node1->parent = parent2;
    node2->parent = parent1;

    auto node1L = node1->left;
    auto node1R = node1->right;
    auto node2L = node2->left;
    auto node2R = node2->right;

    node1->left = node2L;
    node1->right = node2R;
    node2->left = node1L;
    node2->right = node1R;

    if (node1L)
        node1L->parent = node2;
    if (node1R)
        node1R->parent = node2;
    if (node2L)
        node2L->parent = node1;
    if (node2R)
        node2R->parent = node1;

    std::cout << "После смены мест: ";
    print();
}

int
BinTree::balance(BinNode* node) const
{
    if (!node)
        return 0;
    auto rHeight = (node->right) ? node->right->height : 0;
    auto lHeight = (node->left) ? node->left->height : 0;
    return rHeight - lHeight;
}

void
BinTree::recalcHeight(BinNode* node)
{
    if (!node)
        return;

    int leftHeight = 0;
    int rightHeight = 0;

    if (node->left) {
        if (node->left->data == node->data) {
            leftHeight = 1;
        } else {
            leftHeight = node->left->height;
        }
    }
    if (node->right) {
        rightHeight = node->right->height;
    }

    if (leftHeight < rightHeight) {
        node->height = rightHeight + 1;
    } else {
        node->height = leftHeight + 1;
    }
}

BinNodeQueue*
BinTree::makeQueueInWidth() const
{
    auto out = new BinNodeQueue();

    if (empty())
        return out;

    auto currentLevel = new BinNodeQueue();
    auto nextLevel = new BinNodeQueue();

    nextLevel->push(m_root);

    while (!nextLevel->empty()) {
        //Перенос в текущей уровень из последующего
        while (!nextLevel->empty()) {
            currentLevel->push(nextLevel->pop());
        }

        while (!currentLevel->empty()) {
            auto node = currentLevel->pop();
            out->push(node);

            if (node->left) {
                nextLevel->push(node->left);
            }
            if (node->right) {
                nextLevel->push(node->right);
            }
        }
    }

    delete currentLevel;
    delete nextLevel;

    return out;
}

void
BinTree::normalize(BinNode* node)
{
    if (!node)
        return;

    std::cout << "---Нормализация " << node->data << "(" << node << ")---\n";

    auto curNode = node;

    while (curNode) {

        recalcHeight(curNode);

        if (balance(curNode) == -2) {
            if (balance(curNode->left) <= 0)
                smallRightTurn(curNode);
            else
                bigRightTurn(curNode);
        } else if (balance(curNode) == 2) {

            if (balance(curNode->right) >= 0)
                smallLeftTurn(curNode);
            else
                bigLeftTurn(curNode);
        }

        curNode = curNode->parent;
    }

    std::cout << "После нормализации: ";
    print();
}

void
BinTree::bigLeftTurn(BinNode* node)
{
    std::cout << "-Большое левое вращение для " << node->data << "(" << node
              << ")-\n";
    auto nodeA = node;
    auto nodeB = node->right;
    auto nodeC = nodeB->left;

    smallRightTurn(nodeB);
    smallLeftTurn(nodeA);

    recalcHeight(nodeA);
    recalcHeight(nodeB);
    recalcHeight(nodeC); //< Считать последним
}

void
BinTree::smallLeftTurn(BinNode* node)
{
    std::cout << "Малое левое вращение для " << node->data << "(" << node
              << ")\n";

    auto nodeA = node;
    auto parent = node->parent;
    auto nodeB = nodeA->right;

    nodeA->right = nodeB->left;
    if (nodeA->right)
        nodeA->right->parent = nodeA;
    nodeA->parent = nodeB;
    nodeB->left = nodeA;
    nodeB->parent = parent;

    recalcHeight(nodeA);
    recalcHeight(nodeB);

    if (parent == nullptr)
        m_root = nodeB;
    else if (nodeA == parent->left)
        parent->left = nodeB;
    else if (nodeA == parent->right)
        parent->right = nodeB;
    else
        throw std::runtime_error(
          "Imposible to have no parent at this situation");
}

void
BinTree::bigRightTurn(BinNode* node)
{
    std::cout << "-Большое правое вращение для " << node->data << "(" << node
              << ")-\n";
    auto nodeA = node;
    auto nodeB = node->left;
    auto nodeC = nodeB->right;

    smallLeftTurn(nodeB);
    smallRightTurn(nodeA);

    recalcHeight(nodeA);
    recalcHeight(nodeB);
    recalcHeight(nodeC); //< Считать последним
}

void
BinTree::smallRightTurn(BinNode* node)
{
    std::cout << "Малое правое вращение для " << node->data << "(" << node
              << ")\n";
    auto nodeA = node;
    auto parent = node->parent;
    auto nodeB = nodeA->left;

    nodeA->left = nodeB->right;
    if (nodeA->left)
        nodeA->left->parent = nodeA;
    nodeA->parent = nodeB;
    nodeB->right = nodeA;
    nodeB->parent = parent;

    recalcHeight(nodeA);
    recalcHeight(nodeB);

    if (parent == nullptr)
        m_root = nodeB;
    else if (nodeA == parent->left)
        parent->left = nodeB;
    else if (nodeA == parent->right)
        parent->right = nodeB;
    else
        throw std::runtime_error(
          "Imposible to have no parent at this situation");
}

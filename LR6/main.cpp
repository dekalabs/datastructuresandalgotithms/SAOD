#include "bintree.hpp"
#include <iostream>
#include <string>

using namespace std;

char
inputMenu()
{

    cout << "Выберите действие: \n\t"
            "1 - Добавление элемента\n\t"
            "2 - Удаление элемента\n\t"
            "3 - Поиск\n\t"
            "4 - Вывод дерева\n\t"
            "5 - Удалить нечетные элементы при обходе в ширину\n\t"
            "6 - Выход\n\t"
            "\n";
    char in;
    cin >> in;

    return in;
}

int
inputInt()
{
    cin.clear();
    cin.ignore(cin.rdbuf()->in_avail());
    cin.sync();

    std::cout << "Введите целое число : \n";

    char ch;
    while (true) {
        std::string tmpNumber;
        cin.get(ch);
        while (ch != '\n') {
            tmpNumber += ch;
            cin.get(ch);
        }
        try {
            int timed = std::stoi(tmpNumber);
            return timed;
        } catch (const std::exception& e) {
            (void)e;
            continue;
        }
    }
}

void
addElement(BinTree& tree)
{
    cout << "Введите число для добавления : \n";
    BinTreeData data = inputInt();
    tree.add(data);
}

void
removeElement(BinTree& tree)
{
    cout << "Введите число для удаления : \n";
    BinTreeData data = inputInt();
    tree.remove(data);
}

void
searchElement(BinTree& tree)
{
    cout << "Введите число для поиска : \n";
    BinTreeData data = inputInt();
    tree.find(data);
}

int
main()
{

    BinTree tree;
    bool shouldExit = false;

    while (!shouldExit) {
        char input = inputMenu();
        switch (input) {
            case '1':
                addElement(tree);
                break;
            case '2':
                removeElement(tree);
                break;
            case '3':
                searchElement(tree);
                break;
            case '4':
                tree.print();
                break;
            case '5':
                tree.removeOdds();
                break;
            case '6':
                shouldExit = true;
                break;
            default:
                cout << "Пункт меню введен не верно.\n";
                break;
        }
    }

    return 0;
}

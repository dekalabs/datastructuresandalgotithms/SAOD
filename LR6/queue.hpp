#ifndef QUEUE_HPP
#define QUEUE_HPP

#include <stdexcept>

template<class QueueData>
class Queue
{
  private:
    struct QueueNode
    {
        QueueData data;
        QueueNode* next = nullptr;
        QueueNode* back = nullptr;
    };

    QueueNode* m_begin;

  public:
    Queue();
    Queue(const Queue& other) = delete;
    ~Queue();

    void push(QueueData data);
    QueueData pop();

    bool empty() const { return !m_begin; }
};

template<class DequeData>
Queue<DequeData>::Queue()
  : m_begin(nullptr)
{}

template<class DequeData>
Queue<DequeData>::~Queue()
{
    while (!empty()) {
        pop();
    }
}

template<class DequeData>
void
Queue<DequeData>::push(DequeData data)
{
    if (empty()) {
        auto node = new QueueNode();
        node->data = data;
        node->next = node;
        node->back = node;
        m_begin = node;
    } else {
        auto node = new QueueNode();
        auto lastNode = m_begin->back;
        lastNode->next = node;
        m_begin->back = node;
        node->next = m_begin;
        node->back = lastNode;
        node->data = data;
    }
}

template<class DequeData>
DequeData
Queue<DequeData>::pop()
{
    if (empty())
        throw std::runtime_error("Poping from empty deque");

    DequeData out = m_begin->data;
    if (m_begin->next == m_begin) {
        delete m_begin;
        m_begin = nullptr;
    } else {
        auto new_begin = m_begin->next;

        m_begin->back->next = new_begin;
        new_begin->back = m_begin->back;

        delete m_begin;
        m_begin = new_begin;
    }

    return out;
}

#endif // QUEUE_HPP

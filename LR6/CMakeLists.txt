cmake_minimum_required(VERSION 3.12)

project(LR6)

set(LR6_HDRS
    bintree.hpp
    queue.hpp
    )
set(LR6_SRC
    main.cpp
    bintree.cpp
    )

add_executable(LR6 ${LR6_HDRS} ${LR6_SRC})

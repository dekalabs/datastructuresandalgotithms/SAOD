#ifndef BINTREE_HPP
#define BINTREE_HPP

#include "queue.hpp"

typedef int BinTreeData;

struct BinNode
{
    BinTreeData data = 0;
    BinNode* left = nullptr;
    BinNode* right = nullptr;
    BinNode* parent = nullptr;

    int height = 1;
};

typedef Queue<BinNode*> BinNodeQueue;

class BinTree
{
  private:
    BinNode* m_root;

  public:
    BinTree();
    BinTree(const BinTree& other) = delete;
    ~BinTree();

    void add(BinTreeData data);

    BinNode* find(BinTreeData data) const;

    void remove(BinTreeData data);

    void print() const;

    bool empty() const { return !m_root; }
    bool leaf(BinNode* node) const { return !node->left && !node->right; }
    bool contains(BinNode* node) const;

    //Возращает -1 - слева, 0 - не является родитилем, 1 справа
    int parentTo(BinNode* parent, BinNode* node) const;

    void removeOdds();

  private:
    void remove(BinNode* node);

    void swap(BinNode* node1, BinNode* node2);

    int balance(BinNode* node) const;
    void recalcHeight(BinNode* node);

    BinNodeQueue* makeQueueInWidth() const;

    void normalize(BinNode* node);

    void bigLeftTurn(BinNode* node);
    void smallLeftTurn(BinNode* node);

    void bigRightTurn(BinNode* node);
    void smallRightTurn(BinNode* node);
};

#endif // BINTREE_HPP

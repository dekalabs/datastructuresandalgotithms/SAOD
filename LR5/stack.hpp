#ifndef STACK_HPP
#define STACK_HPP

#include <cstddef>

typedef size_t StackType;

class Stack
{
  private:
    struct OneWayStackNode
    {
        StackType data = 0;
        OneWayStackNode* next = nullptr;
    };

    OneWayStackNode* m_top;

  public:
    Stack();
    Stack(const Stack& other) = delete;
    ~Stack();

    void push(StackType data);
    StackType pop();
    bool isEmpty() const { return !m_top; }
};

#endif // STACK_HPP

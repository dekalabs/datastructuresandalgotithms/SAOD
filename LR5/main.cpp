#include "sortedarray.hpp"
#include <iostream>
#include <string>

using namespace std;

char
inputMenu()
{

    cout << "Выберите действие: \n\t"
            "1 - Добавление элемента\n\t"
            "2 - Поиск по порядковой позиции\n\t"
            "3 - Поиск по содержимому\n\t"
            "4 - Удаление элемента\n\t"
            "5 - Вывод массива\n\t"
            "6 - Число уникальных элементов\n\t"
            "7 - Выход\n\t"
            "\n";
    char in;
    cin >> in;

    return in;
}

int
inputInt()
{
    cin.clear();
    cin.ignore(cin.rdbuf()->in_avail());
    cin.sync();

    std::cout << "Введите целое число : ";

    char ch;
    while (true) {
        std::string tmpNumber;
        cin.get(ch);
        while (ch != '\n') {
            tmpNumber += ch;
            cin.get(ch);
        }
        try {
            int timed = std::stoi(tmpNumber);
            return timed;
        } catch (const std::exception& e) {
            (void)e;
            std::cout << "Введите целое число : ";
            continue;
        }
    }
}

size_t
inputPos()
{
    cin.clear();
    cin.ignore(cin.rdbuf()->in_avail());
    cin.sync();

    std::cout << "Введите целое положительное число : ";

    char ch;
    while (true) {
        std::string tmpNumber;
        cin.get(ch);
        while (ch != '\n') {
            tmpNumber += ch;
            cin.get(ch);
        }
        try {
            size_t timed = std::stoull(tmpNumber);
            return timed;
        } catch (const std::exception& e) {
            (void)e;
            std::cout << "Введите целое положительное число : ";
            continue;
        }
    }
}

void
addElement(SortedArray& arr)
{
    cout << "Введите число для добавления: \n";
    int data = inputInt();

    arr.insert(data);

    cout << "Отсортировано : ";
    arr.print();
}

void
searchById(const SortedArray& arr)
{
    cout << "Введите позицию: \n";
    size_t pos = inputPos();
    try {
        cout << "Элемент на " << pos << " позиции : " << arr.at(pos) << ".\n";
    } catch (const std::exception& e) {
        (void)e;
        cout << "Выход за пределы массива.\n";
    }
}

void
searchByData(const SortedArray& arr)
{
    cout << "Введите число для поиска: \n";
    int data = inputInt();

    size_t pos = 0;

    if (arr.find(data, &pos)) {
        cout << "Элемент со значением '" << data
             << "' обнаружен в позиции : " << pos << ".\n";
    } else {
        cout << "Элемент со значением '" << data << "' не обнаружен.\n";
    }
}

void
removeElement(SortedArray& arr)
{
    cout << "Введите позицию: \n";
    size_t pos = inputPos();
    try {
        arr.remove(pos);
        cout << "Элемент на " << pos << " позиции удален\n";
    } catch (const std::exception& e) {
        (void)e;
        cout << "Выход за пределы массива.\n";
        return;
    }
}

int
main()
{
    bool shouldExit = false;
    SortedArray arr;

    while (!shouldExit) {
        char input = inputMenu();
        switch (input) {
            case '1':
                addElement(arr);
                break;
            case '2':
                searchById(arr);
                break;
            case '3':
                searchByData(arr);
                break;
            case '4':
                removeElement(arr);
                break;
            case '5':
                arr.print();
                break;
            case '6':
                cout << "Число уникальных значений: " << arr.uniqueCount()
                     << "\n";
                break;
            case '7':
                shouldExit = true;
                break;
            default:
                cout << "Пункт меню введен не верно.\n";
                break;
        }
    }

    return 0;
}

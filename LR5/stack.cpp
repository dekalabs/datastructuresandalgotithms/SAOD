#include "stack.hpp"
#include <stdexcept>

Stack::Stack()
  : m_top(nullptr)
{}

Stack::~Stack()
{
    while (m_top) {
        pop();
    }
}

void
Stack::push(StackType data)
{
    auto new_node = new OneWayStackNode();
    new_node->data = data;
    if (!m_top) {
        new_node->next = nullptr;
    } else {
        new_node->next = m_top;
    }
    m_top = new_node;
}

StackType
Stack::pop()
{
    if (!m_top)
        throw std::runtime_error("Poping from empty stack");
    StackType out = m_top->data;
    OneWayStackNode* our_top = m_top;
    if (our_top->next) {
        m_top = our_top->next;
    } else {
        m_top = nullptr;
    }
    delete our_top;
    our_top = nullptr;
    return out;
}

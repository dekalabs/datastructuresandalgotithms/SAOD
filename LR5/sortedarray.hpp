#ifndef SORTEDARRAY_H
#define SORTEDARRAY_H

#include <cstddef>

typedef int TypeData;

class SortedArray
{
  private:
    TypeData* m_array;
    size_t m_size;

  public:
    SortedArray();
    SortedArray(const SortedArray& other) = delete;
    ~SortedArray();

    void resize(size_t new_size);

    void insert(TypeData data);
    void remove(size_t pos);

    int& at(size_t pos) const;
    size_t size() const { return m_size; }

    void swap(size_t pos1, size_t pos2);

    bool find(TypeData data, size_t* pos = nullptr) const;
    void sort();

    void print() const;

    size_t uniqueCount() const;

  private:
    bool findMedIndex(size_t& out, size_t begin, size_t end);
    size_t splitPosition(size_t medId, size_t begin, size_t end);
};

#endif // SORTEDARRAY_H

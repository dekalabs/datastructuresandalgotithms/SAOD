#include "sortedarray.hpp"
#include "stack.hpp"
#include <iostream>
#include <stdexcept>

SortedArray::SortedArray()
  : m_array(nullptr)
  , m_size(0)
{}

SortedArray::~SortedArray()
{
    m_size = 0;
    if (m_array)
        delete[] m_array;
    m_array = nullptr;
}

void
SortedArray::resize(size_t new_size)
{
    if (new_size == 0) {
        if (m_array)
            delete[] m_array;
        m_array = nullptr;
        m_size = 0;
        return;
    }

    int* arr = new int[new_size];
    if (m_size > new_size) {
        for (size_t pos = 0; pos < new_size; pos++) {
            arr[pos] = m_array[pos];
        }
    } else {
        for (size_t pos = 0; pos < m_size; pos++) {
            arr[pos] = m_array[pos];
        }
    }

    delete[] m_array;
    m_array = arr;
    m_size = new_size;
}

void
SortedArray::insert(TypeData data)
{
    std::cout << "Добавлено в конец " << data << " : ";
    print();
    resize(m_size + 1);
    m_array[m_size - 1] = data;
    std::cout << "\t\t --> ";
    print();
    sort();
}

void
SortedArray::remove(size_t pos)
{
    if (!(pos < m_size) || m_size == 0)
        throw std::invalid_argument("pos is invalid");

    //Скидываем элемент в конец
    for (size_t i = pos + 1; i < m_size; i++) {
        swap(i - 1, i);
    }
    resize(m_size - 1);
}

int&
SortedArray::at(size_t pos) const
{
    if (!(pos < m_size))
        throw std::invalid_argument("pos is invalid");

    return m_array[pos];
}

void
SortedArray::swap(size_t pos1, size_t pos2)
{
    if (!(pos1 < m_size))
        throw std::invalid_argument("pos1 is invalid");
    if (!(pos2 < m_size))
        throw std::invalid_argument("pos2 is invalid");

    std::cout << "Смена мест (" << pos1 << ", " << pos2 << ") : ";
    print();
    int tmp = m_array[pos2];
    m_array[pos2] = m_array[pos1];
    m_array[pos1] = tmp;
    std::cout << "\t\t --> ";
    print();
}

bool
SortedArray::find(TypeData data, size_t* pos) const
{
    if (m_size == 0)
        return false;
    size_t left = 0;
    size_t right = m_size - 1;
    size_t med = 0;

    while (left != right) {
        med = (left + right) / 2;
        if (m_array[med] < data) {
            left = med + 1;
        } else {
            right = med;
        }
    }

    if (m_array[left] == data) {
        if (pos)
            *pos = left;
        return true;
    }
    return false;
}

void
SortedArray::sort()
{

    Stack beginStack;
    beginStack.push(0);
    Stack endStack;
    endStack.push(m_size - 1);

    while (!beginStack.isEmpty()) {

        size_t begin = beginStack.pop();
        size_t end = endStack.pop();

        if (begin >= end) {
            continue;
        }

        std::cout << "--Сортировка с " << begin << " до " << end << "--\n";

        size_t medID = 0;

        if (findMedIndex(medID, begin, end)) {

            std::cout << "\tСредний элемент : " << m_array[medID] << "\n";

            size_t splitPos = splitPosition(medID, begin, end);
            std::cout << "\tТочка разрыва : " << splitPos << "\n";

            beginStack.push(begin);
            endStack.push((splitPos > 0) ? splitPos - 1 : 0);

            beginStack.push(splitPos + 1);
            endStack.push(end);
        }
    }
}

void
SortedArray::print() const
{
    std::cout << "Массив: ";
    if (m_size == 0) {
        std::cout << " пуст\n";
        return;
    }

    for (size_t pos = 0; pos < m_size; pos++) {
        std::cout << m_array[pos] << " ";
    }

    std::cout << "\n";
}

size_t
SortedArray::uniqueCount() const
{
    if (m_size == 0)
        return 0;
    size_t count = 1;
    for (size_t pos = 1; pos < m_size; pos++) {
        if (m_array[pos - 1] != m_array[pos])
            count++;
    }
    return count;
}

bool
SortedArray::findMedIndex(size_t& out, size_t begin, size_t end)
{
    if ((end - begin) == 0)
        return false;

    out = begin + (end - begin) / 2;
    return true;
}

size_t
SortedArray::splitPosition(size_t medId, size_t begin, size_t end)
{
    if (begin - end <= 1)
        return 0;

    TypeData medData = m_array[medId];

    size_t left = begin;
    size_t right = end;

    while (true) {
        while (m_array[left] < medData && left <= end)
            left++;
        while (m_array[right] > medData && begin <= right)
            right--;

        if (left >= right) {
            return left;
        }

        if (m_array[left] != m_array[right]) {
            swap(left, right);
        } else {
            left++;
        }
    }
}

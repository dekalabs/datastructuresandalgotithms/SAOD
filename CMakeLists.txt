cmake_minimum_required(VERSION 3.12)

project(LR)

add_subdirectory(LR1)
add_subdirectory(LR2)
add_subdirectory(LR3)
add_subdirectory(LR4)
add_subdirectory(LR5)
add_subdirectory(LR6)
add_subdirectory(LR7)

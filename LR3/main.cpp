#include "processor.hpp"
#include "taskqueue.hpp"
#include "taskstack.hpp"
#include <ctime>

#include <iostream>
using namespace std;

void
generateStackWithTasks(TaskStack& stack)
{
    uint count = rand() % 3 + 1;
    for (uint i = 0; i < count; i++) {
        TaskPtr new_task = generateTask();
        stack.push(new_task);
    }
}

char
inputMenu()
{
    while (true) {
        cout << "Выберите действие: \n\t"
                "1 - Добавить задачу\n\t"
                "2 - Добавить случайные задачи\n\t"
                "3 - Начать обработку задач\n\t"
                "4 - Выход\n\t"
                "\n";
        char in;
        cin >> in;
        if (in == '1' || in == '2' || in == '3' || in == '4')
            return in;
        cout << "Такого пункта меню нет\n";
    }
}

void
inputTask(TaskStack& stack)
{
    String nameTask;
    uint neededTicks = 0;
    std::cout << "Введите название: ";

    cin.clear();
    cin.ignore(cin.rdbuf()->in_avail());
    cin.sync();

    char ch;
    cin.get(ch);
    while (ch != '\n') {
        nameTask += ch;
        cin.get(ch);
    }

    cin.clear();
    cin.ignore(cin.rdbuf()->in_avail());
    cin.sync();

    std::cout << "Введите число тиков: ";

    while (true) {
        String tmpNumber;
        cin.get(ch);
        while (ch != '\n') {
            tmpNumber += ch;
            cin.get(ch);
        }
        try {
            long timed = std::stol(tmpNumber);
            if (timed < 0) {
                std::cout << "Число должно быть положительным\n";
                std::cout << "Введите число тиков: ";
                continue;
            }
            neededTicks = static_cast<uint>(timed);
            break;
        } catch (const std::exception& e) {
            (void)e;
            std::cout << "Введите число тиков: ";
            continue;
        }
    }

    TaskPtr new_task(new Task());
    new_task->name = nameTask;
    new_task->requiredTicks = static_cast<int>(neededTicks);
    stack.push(new_task);
}

void
process(TaskStack& stackBegin)
{
    Processor p0("Процессор P0"), p1("Процессор P1");

    TaskStack& stackForP0 = stackBegin;
    TaskQueue queueForP1;

    TaskPtr new_task = stackForP0.pop();
    p0.setTask(new_task);

    while (!p0.isFree() || !stackForP0.isEmpty() || !p1.isFree() ||
           !queueForP1.isEmpty()) {

        p0.update();
        p1.update();
        //Если очереди или cтаки пусты, то старая задача просто будет
        //отсоединена от процессора
        if (p0.isFree()) {
            TaskPtr new_task = stackForP0.pop();
            TaskPtr old_task = p0.setTask(new_task);
            stackForP0.print();
            if (old_task) {
                old_task->ticksProcessed = 0; //< Сброс задачи в начало
                old_task->startTime = -1;
                queueForP1.push(old_task);
            }
        }

        if (p1.isFree()) {
            TaskPtr new_task = queueForP1.pop();
            p1.setTask(new_task);
            queueForP1.print();
        }
    }
}

int
main()
{
    srand(static_cast<unsigned int>(time(nullptr)));

    TaskStack stack;

    bool shouldExit = false;
    stack.print();

    while (!shouldExit) {
        char input = inputMenu();
        switch (input) {
            case '1':
                inputTask(stack);
                stack.print();
                break;
            case '2':
                generateStackWithTasks(stack);
                stack.print();
                break;
            case '3':
                process(stack);
                break;
            case '4':
                shouldExit = true;
                break;
            default:
                cout << "Пункт меню введен не верно.\n";
                break;
        }
    }

    return 0;
}

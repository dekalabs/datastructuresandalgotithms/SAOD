#include "taskstack.hpp"

TaskStack::TaskStack()
  : m_top(nullptr)
{}

TaskStack::~TaskStack()
{
    //Выгрузка всего стека
    while (m_top) {
        pop();
    }
}

void
TaskStack::push(TaskPtr task)
{
    auto new_node = new OneWayTaskNode();
    new_node->data = task;
    if (!m_top) {
        new_node->next = nullptr;
    } else {
        new_node->next = m_top;
    }
    m_top = new_node;
}

TaskPtr
TaskStack::pop()
{
    if (!m_top)
        return nullptr;
    TaskPtr out = m_top->data;
    OneWayTaskNode* our_top = m_top;
    if (our_top->next) {
        m_top = our_top->next;
    } else {
        m_top = nullptr;
    }
    delete our_top;
    our_top = nullptr;
    return out;
}

void
TaskStack::print() const
{
    if (!m_top) {
        std::cout << "Стек пуст\n";
        return;
    }
    std::cout << "Стек содержит элементы : \n";
    OneWayTaskNode* currentNode = m_top;
    while (currentNode) {
        std::cout << "\t";
        printTask(*(currentNode->data));
        currentNode = currentNode->next;
    }
}

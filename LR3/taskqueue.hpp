#ifndef TASKQUEUE_HPP
#define TASKQUEUE_HPP

#include "task.hpp"

class TaskQueue
{
  private:
    struct DuoWayTaskNode
    {
        TaskPtr data = nullptr;
        DuoWayTaskNode* next = nullptr;
        DuoWayTaskNode* back = nullptr;
    };
    DuoWayTaskNode* m_begin;

  public:
    TaskQueue();
    TaskQueue(const TaskQueue& other) = delete;
    ~TaskQueue();

    void push(TaskPtr new_data);
    TaskPtr pop();

    bool isEmpty() const { return !m_begin; }

    void print() const;
};

#endif // TASKQUEUE_HPP

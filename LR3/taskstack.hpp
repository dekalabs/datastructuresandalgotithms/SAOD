#ifndef TASKSTACK_HPP
#define TASKSTACK_HPP

#include "task.hpp"

class TaskStack
{
    struct OneWayTaskNode
    {
        TaskPtr data = nullptr;
        OneWayTaskNode* next = nullptr;
    };

    OneWayTaskNode* m_top;

  public:
    TaskStack();
    TaskStack(const TaskStack& other) = delete;
    ~TaskStack();

    void push(TaskPtr task); ///< Добавляет в стек
    TaskPtr pop(); ///< Берет из стека. Если стек пуст возврщает

    bool isEmpty() const { return !m_top; }

    void print() const;
};

#endif // TASKSTACK_HPP

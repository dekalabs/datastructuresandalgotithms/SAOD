#include "processor.hpp"

Processor::Processor(const String& name)
  : m_processorName(name)
  , m_ticksPassed(0)
  , m_currentTask(nullptr)
{}

Processor::~Processor() {}

TaskPtr
Processor::setTask(TaskPtr task)
{
    if (!task) {
        if (m_currentTask) {
            std::cout << m_processorName
                      << " : Отсоединение обработаного задания - ";
            printTask(*m_currentTask);
        }
        TaskPtr out = m_currentTask;
        m_currentTask = nullptr;
        return out;
    }
    if (this->isFree()) {
        std::cout << m_processorName << " : Установка нового задания - ";
        printTask(*task);
        TaskPtr out = m_currentTask;
        m_currentTask = task;
        m_currentTask->startTime = m_ticksPassed;
        return out;
    }
    return nullptr;
}

void
Processor::update()
{
    std::cout << m_processorName << " : Тик #" << m_ticksPassed << "\n";
    if (m_currentTask) {
        m_currentTask->ticksProcessed++;

        std::cout << "\tПосле обработки в данном тике : \n\t";
        printTask(*m_currentTask);
    } else {
        std::cout << "Нет задач\n";
    }

    m_ticksPassed++;
}

bool
Processor::isFree() const
{
    if (m_currentTask == nullptr)
        return true;
    if (m_currentTask->requiredTicks <= m_currentTask->ticksProcessed)
        return true;
    return false;
}

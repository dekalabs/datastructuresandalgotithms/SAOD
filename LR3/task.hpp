#ifndef TASK
#define TASK

#include <iostream>
#include <memory>
#include <string>

// typedef unsigned long uint;
typedef std::string String;

struct Task
{
    String name = "Unnamed task";
    long startTime = -1;
    uint requiredTicks = 0;
    uint ticksProcessed = 0;
};

typedef std::shared_ptr<Task> TaskPtr;

static TaskPtr
generateTask()
{
    TaskPtr new_task = TaskPtr(new Task());
    new_task->name = std::to_string(rand()); //< Имя будет случайным числом
    new_task->requiredTicks = rand() % 9 + 1; //< 1-10
    return new_task;
}

static void
printTask(const Task& task)
{
    std::cout << "Название: \"" << task.name << "\" ";
    if (task.startTime == -1) {
        std::cout << "Процесс ещё не начал обрабатываться; ";
    } else {
        std::cout << "Процесс обрабатывается с " << task.startTime << " тика; ";
    }
    if (task.requiredTicks > task.ticksProcessed) {
        std::cout << task.requiredTicks - task.ticksProcessed
                  << " тиков до конца обработки.\n";
    } else {
        std::cout << "задача обработана.\n";
    }
}

#endif //! TASK

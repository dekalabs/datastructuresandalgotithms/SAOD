#include "taskqueue.hpp"

#include <iostream>

TaskQueue::TaskQueue()
  : m_begin(nullptr)
{}

TaskQueue::~TaskQueue()
{
    while (m_begin) {
        pop();
    }
}

void
TaskQueue::push(TaskPtr new_data)
{
    //Добавляем в конец списка
    if (!m_begin) {
        auto new_node = new DuoWayTaskNode();
        new_node->data = new_data;
        new_node->next = new_node;
        new_node->back = new_node;
        m_begin = new_node;
    } else {
        auto new_node = new DuoWayTaskNode();
        new_node->data = new_data;
        new_node->next = m_begin;
        new_node->back = m_begin->back;
        m_begin->back->next = new_node;
        m_begin->back = new_node;
    }
}

TaskPtr
TaskQueue::pop()
{
    if (!m_begin)
        return nullptr;

    //Извлекаем из начала
    DuoWayTaskNode* our_top = m_begin;
    TaskPtr out = our_top->data;
    if (m_begin->next == m_begin) //< Последний элемент остался
    {
        m_begin = nullptr;
    } else {
        m_begin->back->next = m_begin->next;
        m_begin->next->back = m_begin->back;
        m_begin = m_begin->next;
    }

    delete our_top;
    our_top = nullptr;

    return out;
}

void
TaskQueue::print() const
{
    if (!m_begin) {
        std::cout << "Очередь пустая\n";
        return;
    }
    std::cout << "Очередь содержит элементы : \n";
    DuoWayTaskNode* currentNode = m_begin;
    while (currentNode->next != m_begin) {
        std::cout << "\t";
        printTask(*(currentNode->data));
        currentNode = currentNode->next;
    }
    std::cout << "\t";
    printTask(*(currentNode->data));
}

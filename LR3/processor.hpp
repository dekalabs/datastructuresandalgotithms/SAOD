#ifndef PROCESSOR
#define PROCESSOR

#include "task.hpp"

class Processor
{
  private:
    String m_processorName;
    int m_ticksPassed;
    TaskPtr m_currentTask;

  public:
    Processor(const String& name = "Процессор");
    Processor(const Processor& other) = delete; //< Запрещаем копирование
    ~Processor();

    /**
     * Устанавливает новую задачу, если процессор свободен
     * Возвращает уже обработанную задачу
     */
    TaskPtr setTask(TaskPtr task);

    /**
     * Обрабатывает 1 тик
     */
    void update();

    /**
     * Проверяет свободен ли процессор
     */
    bool isFree() const;
};

#endif //! PROCESSOR

#ifndef GRAF_HPP
#define GRAF_HPP

#include "array.hpp"
#include <string>

typedef std::string Vertex;

struct DualWayArc
{
    Vertex first;
    Vertex second;
};

bool
operator==(const DualWayArc& left, const DualWayArc& right);

class DualWayGraf
{
  private:
    bool** m_matrix;
    size_t m_xSize;
    size_t m_ySize;

    Array<Vertex> m_vertexArray; //Используются для определения идентификатора
    Array<DualWayArc> m_arcArray;

  public:
    DualWayGraf();
    DualWayGraf(const DualWayGraf& other) = delete;
    ~DualWayGraf();

    bool addVertex(const Vertex& name);
    void delVertex(const Vertex& name);
    bool addConnect(const DualWayArc& arc);
    void delConnect(const DualWayArc& arc);

    bool isHavePath(const Vertex& first, const Vertex& last) const;

    void printMatrix() const;
    void printVertexes() const;
    void printConnections() const;

    void print() const;

    Array<Vertex> getVertexes() const { return m_vertexArray; }

    size_t getIDByVertex(const Vertex& vert) const;
    size_t getIDByConnection(const DualWayArc& arc) const;

  private:
    void resizeMatrix(size_t xSize, size_t ySize);

    Array<DualWayArc> connectionsTo(const Vertex& vert) const;
    Array<Vertex> vertexesFrom(const Vertex& vert) const;

    void rebuildMatrix();
};

#endif //! GRAF_HPP

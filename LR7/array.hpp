#ifndef ARRAY_HPP
#define ARRAY_HPP

#include <cstddef>
#include <stdexcept>

template<typename DataType>
class Array
{
  private:
    DataType* m_array;
    size_t m_size;

  public:
    Array();
    Array(const Array& other);
    ~Array();

    void pushBack(const DataType& data);

    DataType& at(size_t pos) const;
    void swap(size_t pos1, size_t pos2);

    void remove(size_t pos);

    bool find(const DataType& data, size_t* pos = nullptr) const;

    bool contains(const DataType& data) const;

    size_t size() const { return m_size; }

  private:
    void resize(size_t new_size);
};

template<typename DataType>
Array<DataType>::Array()
  : m_array(nullptr)
  , m_size(0)
{}

template<typename DataType>
Array<DataType>::Array(const Array& other)
  : m_array(nullptr)
  , m_size(0)
{
    resize(other.m_size);
    for (size_t i = 0; i < other.m_size; i++) {
        m_array[i] = other.m_array[i];
    }
}

template<typename DataType>
Array<DataType>::~Array()
{
    m_size = 0;
    if (m_array)
        delete[] m_array;
    m_array = nullptr;
}

template<typename DataType>
void
Array<DataType>::pushBack(const DataType& data)
{
    resize(m_size + 1);
    m_array[m_size - 1] = data;
}

template<typename DataType>
DataType&
Array<DataType>::at(size_t pos) const
{
    if (!(pos < m_size))
        throw std::invalid_argument("pos is invalid");

    return m_array[pos];
}

template<typename DataType>
void
Array<DataType>::swap(size_t pos1, size_t pos2)
{
    if (!(pos1 < m_size))
        throw std::invalid_argument("pos1 is invalid");
    if (!(pos2 < m_size))
        throw std::invalid_argument("pos2 is invalid");

    DataType tmp = m_array[pos2];
    m_array[pos2] = m_array[pos1];
    m_array[pos1] = tmp;
}

template<typename DataType>
void
Array<DataType>::remove(size_t pos)
{
    if (!(pos < m_size) || m_size == 0)
        throw std::invalid_argument("pos is invalid");

    //Скидываем элемент в конец
    for (size_t i = pos + 1; i < m_size; i++) {
        swap(i - 1, i);
    }
    resize(m_size - 1);
}

template<typename DataType>
bool
Array<DataType>::find(const DataType& data, size_t* pos) const
{
    if (m_size == 0)
        return false;

    for (size_t id = 0; id < m_size; id++) {
        if (data == m_array[id]) {
            if (pos)
                *pos = id;
            return true;
        }
    }

    return false;
}

template<typename DataType>
bool
Array<DataType>::contains(const DataType& data) const
{
    if (find(data))
        return true;
    return false;
}

template<typename DataType>
void
Array<DataType>::resize(size_t new_size)
{
    if (new_size == 0) {
        if (m_array)
            delete[] m_array;
        m_array = nullptr;
        m_size = 0;
        return;
    }

    DataType* arr = new DataType[new_size];
    if (m_size > new_size) {
        for (size_t pos = 0; pos < new_size; pos++) {
            arr[pos] = m_array[pos];
        }
    } else {
        for (size_t pos = 0; pos < m_size; pos++) {
            arr[pos] = m_array[pos];
        }
    }

    delete[] m_array;
    m_array = arr;
    m_size = new_size;
}

#endif //! ARRAY_HPP

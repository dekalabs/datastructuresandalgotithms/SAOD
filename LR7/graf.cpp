#include "graf.hpp"
#include <iostream>

DualWayGraf::DualWayGraf()
  : m_matrix(nullptr)
  , m_xSize(0)
  , m_ySize(0)
  , m_vertexArray()
  , m_arcArray()
{}

DualWayGraf::~DualWayGraf()
{
    resizeMatrix(0, 0);
}

bool
DualWayGraf::addVertex(const Vertex& name)
{
    if (m_vertexArray.find(name)) {
        std::cout << "Невозможно добавить вершину, которая уже есть\n";
        return false;
    }

    m_vertexArray.pushBack(name);

    rebuildMatrix();

    return true;
}

void
DualWayGraf::delVertex(const Vertex& name)
{
    size_t idVert = 0;
    if (m_vertexArray.find(name, &idVert)) {

        auto connectionsWithVertex = connectionsTo(name);

        for (size_t id = 0; id < connectionsWithVertex.size(); id++) {
            delConnect(connectionsWithVertex.at(id));
        }

        m_vertexArray.remove(idVert);

        rebuildMatrix();
    }
}

bool
DualWayGraf::addConnect(const DualWayArc& arc)
{
    if (!m_vertexArray.find(arc.first) || !m_vertexArray.find(arc.second)) {
        std::cout
          << "Невозможно добавить связь если отсуствует одна их вершин.\n";
        return false;
    }
    if (m_arcArray.find(arc)) {
        std::cout << "Невозможно добавить уже существующую связь.\n";
        return false;
    }

    m_arcArray.pushBack(arc);

    rebuildMatrix();

    return true;
}

void
DualWayGraf::delConnect(const DualWayArc& arc)
{
    size_t id = 0;

    if (m_arcArray.find(arc, &id)) {
        m_arcArray.remove(id);
    }

    rebuildMatrix();
}

bool
DualWayGraf::isHavePath(const Vertex& first, const Vertex& last) const
{
    Array<Vertex> visited;
    visited.pushBack(first);

    bool somethingAdded = true;

    //Если нечего добавлять можно оканчивать работу
    do {
        somethingAdded = false;
        for (size_t id = 0; id < visited.size(); id++) {
            auto vertex = visited.at(id);
            auto connectedFromVertex = vertexesFrom(vertex);
            for (size_t idConn = 0; idConn < connectedFromVertex.size();
                 idConn++) {
                auto curVert = connectedFromVertex.at(idConn);
                if (!visited.contains(curVert)) { ///< Добавляем только те
                                                  ///< элементы что не добавили
                    somethingAdded = true;
                    visited.pushBack(curVert);
                }
            }
        }
    } while (somethingAdded);

    //Если среди visited есть искомый элемент, то дойти возможно
    if (visited.contains(last)) {
        return true;
    }
    return false;
}

void
DualWayGraf::printMatrix() const
{
    std::cout << "Матрица инцидентости : \n";
    size_t x = 0;
    size_t y = 0;

    for (x = 0; x < m_xSize; x++) {
        std::cout << "\t" << x << " :";
        for (y = 0; y < m_ySize; y++) {
            std::cout << " " << m_matrix[x][y];
        }
        std::cout << "\n";
    }
}

void
DualWayGraf::printVertexes() const
{
    std::cout << "Вершины : \n";

    for (size_t id = 0; id < m_vertexArray.size(); id++) {
        std::cout << "\t" << id << " - " << m_vertexArray.at(id) << "\n";
    }
}

void
DualWayGraf::print() const
{
    printMatrix();
    printVertexes();
    printConnections();
}

void
DualWayGraf::printConnections() const
{
    std::cout << "Связи : \n";

    for (size_t id = 0; id < m_arcArray.size(); id++) {
        std::cout << "\t" << id << " - " << m_arcArray.at(id).first << " <--> "
                  << m_arcArray.at(id).second << "\n";
    }
}

void
DualWayGraf::resizeMatrix(size_t xSize, size_t ySize)
{
    //Просто меняет размер матрицы, не копируюя элементы
    for (size_t x = 0; x < m_xSize; x++) {
        delete[] m_matrix[x];
    }
    delete[] m_matrix;

    m_matrix = new bool*[xSize];

    for (size_t x = 0; x < xSize; x++) {
        m_matrix[x] = new bool[ySize];
    }

    m_xSize = xSize;
    m_ySize = ySize;

    for (size_t x = 0; x < xSize; x++) {
        for (size_t y = 0; y < ySize; y++) {
            m_matrix[x][y] = false;
        }
    }
}

Array<DualWayArc>
DualWayGraf::connectionsTo(const Vertex& vert) const
{
    Array<DualWayArc> out;

    size_t idVert = 0;
    if (m_vertexArray.find(vert, &idVert)) {
        for (size_t x = 0; x < m_arcArray.size(); x++) {
            if (m_matrix[x][idVert]) {
                out.pushBack(m_arcArray.at(x));
            }
        }
    }

    return out;
}

Array<Vertex>
DualWayGraf::vertexesFrom(const Vertex& vert) const
{
    Array<Vertex> out;

    size_t idVert = 0;
    if (m_vertexArray.find(vert, &idVert)) {
        for (size_t x = 0; x < m_arcArray.size(); x++) {
            if (m_matrix[x][idVert]) //Проверка инцидентности по матрице
            {
                if (vert == m_arcArray.at(x).first) { // Выбираем не vert
                    out.pushBack(m_arcArray.at(x).second);
                } else {
                    out.pushBack(m_arcArray.at(x).first);
                }
            }
        }
    }

    return out;
}

void
DualWayGraf::rebuildMatrix()
{
    resizeMatrix(m_arcArray.size(), m_vertexArray.size());

    for (size_t idCon = 0; idCon < m_arcArray.size(); idCon++) {
        auto connection = m_arcArray.at(idCon);
        size_t idVert = 0;
        if (m_vertexArray.find(connection.first, &idVert)) {
            m_matrix[idCon][idVert] = true;
        }
        if (m_vertexArray.find(connection.second, &idVert)) {
            m_matrix[idCon][idVert] = true;
        }
    }
}

bool
operator==(const DualWayArc& left, const DualWayArc& right)
{
    return (left.first == right.first && left.second == right.second) ||
           (left.first == right.second && left.second == right.first);
}

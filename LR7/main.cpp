#include <iostream>
#include <string>

#include "graf.hpp"

using namespace std;

char
inputMenu()
{

    cout << "Выберите действие: \n\t"
            "1 - Добавить человека\n\t"
            "2 - 'Удалить' человека\n\t"
            "3 - Добавить связь\n\t"
            "4 - Удалить связь\n\t"
            "5 - Вывод графа\n\t"
            "6 - Проверить можно ли перезнакомить двух людей\n\t"
            "7 - Выход\n\t"
            "\n";
    char in;
    cin >> in;

    return in;
}

size_t
inputSize()
{
    cin.clear();
    cin.ignore(cin.rdbuf()->in_avail());
    cin.sync();

    std::cout << "Введите целое положительное число : \n";

    char ch = '0';
    while (true) {
        std::string tmpNumber;
        getline(cin, tmpNumber);
        if (tmpNumber.empty())
            continue;
        try {
            size_t timed = std::stoull(tmpNumber);
            return timed;
        } catch (const std::exception& e) {
            (void)e;
            continue;
        }
    }
}

Vertex
chooseVertex(DualWayGraf& graf)
{
    std::cout << "Выберите имя человека: \n";
    graf.printVertexes();
    size_t id = 0;
    do {
        id = inputSize();
    } while (id > graf.getVertexes().size());

    return graf.getVertexes().at(id);
}

void
addVertex(DualWayGraf& graf)
{
    cin.clear();
    cin.ignore(cin.rdbuf()->in_avail());
    cin.sync();

    std::string nameOfVertex;
    std::cout << "Введите имя человека: \n";
    do {
        nameOfVertex.clear();

        getline(cin, nameOfVertex);
    } while (nameOfVertex.size() == 0 || !graf.addVertex(nameOfVertex));
}

void
delVertex(DualWayGraf& graf)
{
    auto vertex = chooseVertex(graf);
    graf.delVertex(vertex);
}

void
addConnection(DualWayGraf& graf)
{
    Vertex first = "";
    Vertex second = "";
    do {
        std::cout << "Выберите имя первого человека : \n";
        first = chooseVertex(graf);
        std::cout << "Выберите имя второго человека : \n";
        second = chooseVertex(graf);

        while (second == first) {
            std::cout << "Выберите разных людей : \n";
            std::cout << "Выберите имя первого человека : \n";
            first = chooseVertex(graf);
            std::cout << "Выберите имя второго человека: \n";
            second = chooseVertex(graf);
        }
    } while (!graf.addConnect({ first, second }));
}

void
delConnection(DualWayGraf& graf)
{
    Vertex first = "";
    Vertex second = "";

    std::cout << "Выберите имя первого человека : \n";
    first = chooseVertex(graf);
    std::cout << "Выберите имя второго человека : \n";
    second = chooseVertex(graf);

    while (second == first) {
        std::cout << "Выберите разных людей : \n";
        std::cout << "Выберите имя первого человека : \n";
        first = chooseVertex(graf);
        std::cout << "Выберите имя второго человека: \n";
        second = chooseVertex(graf);
    }

    graf.delConnect({ first, second });
}

void
checkConnection(DualWayGraf& graf)
{
    std::cout << "Выберите первую вершину : \n";
    Vertex first = chooseVertex(graf);
    std::cout << "Выберите вторую вершину : \n";
    Vertex second = chooseVertex(graf);

    if (first == second) {
        std::cout << "Человек, естественно, знаком с собой.\n";
    } else {

        if (graf.isHavePath(first, second)) {
            std::cout << "Человек \"" << first << "\" может познакомится с \""
                      << second << "\"\n";
        } else {
            std::cout << "Человек \"" << first
                      << "\" НЕ может познакомится с \"" << second << "\"\n";
        }
    }
}

int
main()
{
    DualWayGraf graf;
    bool shouldExit = false;

    while (!shouldExit) {
        char input = inputMenu();
        switch (input) {
            case '1':
                addVertex(graf);
                break;
            case '2':
                delVertex(graf);
                break;
            case '3':
                addConnection(graf);
                break;
            case '4':
                delConnection(graf);
                break;
            case '5':
                graf.print();
                break;
            case '6':
                checkConnection(graf);
                break;
            case '7':
                shouldExit = true;
                break;
            default:
                cout << "Пункт меню введен не верно.\n";
                break;
        }
    }

    return 0;
}
